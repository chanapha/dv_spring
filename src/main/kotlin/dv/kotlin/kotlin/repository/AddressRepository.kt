package dv.kotlin.kotlin.repository

import dv.kotlin.kotlin.entity.Address
import dv.kotlin.kotlin.entity.Product
import org.springframework.data.repository.CrudRepository

interface AddressRepository: CrudRepository<Address,Long>