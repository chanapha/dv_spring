package dv.kotlin.kotlin.repository

import dv.kotlin.kotlin.entity.Manufacturer
import dv.kotlin.kotlin.entity.Product
import org.springframework.data.repository.CrudRepository

interface ManufacturerRepository: CrudRepository<Manufacturer,Long>{
    fun findByName(name: String): Manufacturer
}