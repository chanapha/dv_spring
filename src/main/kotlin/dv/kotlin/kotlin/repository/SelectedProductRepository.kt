package dv.kotlin.kotlin.repository

import dv.kotlin.kotlin.entity.Product
import dv.kotlin.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface SelectedProductRepository: CrudRepository<SelectedProduct,Long> {
     fun findByProduct_NameContainingIgnoreCase(name: String, page: Pageable): Page<SelectedProduct>
}