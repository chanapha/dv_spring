package dv.kotlin.kotlin.repository

import dv.kotlin.kotlin.entity.Product
import dv.kotlin.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository: CrudRepository<ShoppingCart,Long> {
     fun findBySelectedProducts_Product_NameContainingIgnoreCase(name: String, page: Pageable): Page<ShoppingCart>
     fun findBySelectedProducts_Product_NameContainingIgnoreCase(name: String): List<ShoppingCart>
}