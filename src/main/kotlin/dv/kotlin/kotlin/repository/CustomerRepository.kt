package dv.kotlin.kotlin.repository

import dv.kotlin.kotlin.entity.Customer
import dv.kotlin.kotlin.entity.UserStatus
import org.springframework.data.repository.CrudRepository

interface CustomerRepository: CrudRepository<Customer,Long>{
    fun findByName(name: String): Customer
    fun findByNameContaining(name:String): List<Customer>
    fun findByNameEndingWithIgnoreCase(name:String): List<Customer>
    fun findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name:String,email: String ): List<Customer>
    fun findByDefaultAddress_ProvinceContainingIgnoreCase(address: String): List<Customer>
    fun findByUserStatus(status: UserStatus): List<Customer>


}