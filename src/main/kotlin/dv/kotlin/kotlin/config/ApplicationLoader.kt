package dv.kotlin.kotlin.config

import dv.kotlin.kotlin.entity.*
import dv.kotlin.kotlin.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    @Autowired
    lateinit var dataLoader: DataLoader

    @Transactional
    override fun run(args: ApplicationArguments?){
       var manul = manufacturerRepository.save(Manufacturer("CAMT","0000000"))
        var apple = manufacturerRepository.save(Manufacturer("Apple","053123456"))
        var sumsung = manufacturerRepository.save(Manufacturer("SAMSUNG","555666777888"))


        var product1 = productRepository.save(Product("CAMT",
                "The best College in CMU",
                0.0,
                1,
                "http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        manul.products.add(product1)
        product1.manufacturer = manul


        var product2 = productRepository.save(Product("iPhone",
                "It’s a phone",
                28000.00,
                20,
                "https://www.jaymartstore.c"))
        apple.products.add(product2)
        product2.manufacturer = apple



        var product3 = productRepository.save(Product("Prayuth",
                "The best PM ever",
                1.0,
                1,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))
        sumsung.products.add(product3)
        product3.manufacturer = sumsung

        var product4= productRepository.save(Product("Note 9",
                "Other Iphone",
                28001.0,
                10,
                "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))
        sumsung.products.add(product4)
        product4.manufacturer = sumsung


        var customer1 = customerRepository.save(Customer(
                "Lung",
                "pm@go.th",
                UserStatus.ACTIVE
        ))
        customer1.defaultAddress = addressRepository.save(Address(
                "ถนนอนุสาวรีย์ประชาธิปไตย",
                "ดินสอ",
                "เขตดุสิต",
                "กรุงเทพ",
                "10123"
        ))

        var customer2 = customerRepository.save(Customer(
                "ชัชชาติ",
                "chut@taopoon.com",
                UserStatus.ACTIVE
        ))
        customer2.defaultAddress = addressRepository.save(Address(
                "239",
                "มหาวิทยาลัยเชียงใหม่",
                "สุเทพ",
                "เชียงใหม่",
                "50200"
        ))

        var customer3 = customerRepository.save(Customer(
                "ธนาธร",
                "thanathorn@life.com",
                UserStatus.PENDING
        ))
        customer3.defaultAddress = addressRepository.save(Address(
                "ซักที่บนโลก",
                "สุขสันต์",
                "ในเมือง",
                "ขอนแก่น",
                "12457"
        ))

        val selected1 = selectedProductRepository.save(SelectedProduct(4))
        selected1.product = product2

        val selected2 = selectedProductRepository.save(SelectedProduct(1))
        selected2.product = product3

        val selected3 = selectedProductRepository.save(SelectedProduct(1))
        selected3.product = product3

        val selected4 = selectedProductRepository.save(SelectedProduct(1))
        selected4.product = product1

        val selected5 = selectedProductRepository.save(SelectedProduct(2))
        selected5.product = product4

        val shopping1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        shopping1.customer = customer1
        shopping1.selectedProducts.add(selected1)
        shopping1.selectedProducts.add(selected2)

        val shopping2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))
        shopping2.customer = customer2
        shopping2.selectedProducts.add(selected3)
        shopping2.selectedProducts.add(selected4)
        shopping2.selectedProducts.add(selected5)

        dataLoader.loadData()


    }
}




