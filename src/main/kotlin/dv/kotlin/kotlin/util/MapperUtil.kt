package dv.kotlin.kotlin.util

import dv.kotlin.kotlin.entity.*
import dv.kotlin.kotlin.entity.dto.*
import org.mapstruct.*
import org.mapstruct.factory.Mappers

@Mapper (componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source = "manufacturer", target = "manu")
    )

    fun mapProductDto(product: Product?):ProductDto?

    fun mapProductDto(products: List<Product>):List<ProductDto>

    fun mapCustomerDto(customer: Customer?):CustomerDto?

    fun mapCustomerDto(customer: List<Customer>):List<CustomerDto>

    fun mapManufacturer(manu: Manufacturer): ManufacturerDto

    fun mapManufacturer(manu: List<Manufacturer>): List<ManufacturerDto>

    fun mapAddress(address: Address?):AddressDto?

    fun mapAddress(address: List<Address>): List<AddressDto>


    fun mapShoppingCartDto(shoppingCart: ShoppingCart):ShoppingCartDto

    fun mapShoppingCartDto(shoppingCart: List<ShoppingCart>):List<ShoppingCartDto>

    @Mappings(Mapping(source = "product",target = "selectedProduct"))
    fun mapSelectedProductDto(selectedProduct: SelectedProduct?): SelectedProductDto

    fun mapSelectedProductDto(selectedProduct: List<SelectedProduct>):List<SelectedProductDto>

    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer


    @InheritInverseConfiguration
    fun mapAddress(addressDto: AddressDto): Address

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product





}