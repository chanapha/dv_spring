package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Address

interface AddressDao{
     fun save(address: Address): Address

}