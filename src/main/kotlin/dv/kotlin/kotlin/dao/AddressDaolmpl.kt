package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Address
import dv.kotlin.kotlin.repository.AddressRepository
import dv.kotlin.kotlin.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class AddressDaolmpl: AddressDao{
    @Autowired
    lateinit var addressRepository: AddressRepository

    override fun save(address: Address): Address {
        return addressRepository.save(address)

    }


}