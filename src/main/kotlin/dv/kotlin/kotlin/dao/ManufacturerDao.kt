package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Manufacturer

interface ManufacturerDao{
    fun getManufacturers():List<Manufacturer>
    fun save(manufacturer: Manufacturer): Manufacturer
}

