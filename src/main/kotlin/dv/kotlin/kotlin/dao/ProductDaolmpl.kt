package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Manufacturer
import dv.kotlin.kotlin.entity.Product
import dv.kotlin.kotlin.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("mem")
@Repository
class ProductDaolmpl: ProductDao{
    override fun save(product: Product): Product {
        return productRepository.save(product)
    }

    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product> {
        return productRepository.findByNameContainingIgnoreCase(name,PageRequest.of(page,pageSize))
    }

    override fun getProductByManuName(name: String): List<Product> {
        return productRepository.findByManufacturer_NameContainingIgnoreCase(name)
    }

    override fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product> {
        return productRepository.findByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(name,desc)
    }

    override fun getProductByPartialName(name: String): List<Product> {
        return productRepository.findByNameContaining(name)
    }

    @Autowired
    lateinit var productRepository: ProductRepository

    override fun getProductByName(name: String): Product? {
        return productRepository.findByName(name)
    }


    override fun getProduct(): List<Product> {
        return mutableListOf(Product("iPhone", "It’s a phone",28000.00,20,Manufacturer("Apple","053123456"),"https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"),
                Product("Note 9", "The best College in CMU",28001.00,10, Manufacturer("Samsung","555666777888"),"http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"),
                Product("CAMT", "It’s a phone",0.00,1, Manufacturer("CAMT","0000000"),"http://www.camt.cmu.ac.th/th/images/logo.jpg"),
                Product("Prayuth", "The best PM ever",1.00,1, Manufacturer("CAMT","0000000"),"https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))
    }




}