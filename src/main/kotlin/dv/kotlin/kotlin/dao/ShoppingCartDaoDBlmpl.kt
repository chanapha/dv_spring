package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Customer
import dv.kotlin.kotlin.entity.ShoppingCart
import dv.kotlin.kotlin.repository.ShoppingCartRepository
import dv.kotlin.kotlin.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingCartDaoDBlmpl: ShoppingCartDao{
    override fun findByProducrName(query: String): List<ShoppingCart> {
        return shoppingCartRepository
                .findBySelectedProducts_Product_NameContainingIgnoreCase(query)
    }

    override fun getCustomerByNameWithPage(name: String): List<Customer> {
        var c : MutableList<Customer> = ArrayList()
        var shoppingCartByProduct = shoppingCartRepository.findBySelectedProducts_Product_NameContainingIgnoreCase(name)
        for ( each:ShoppingCart in shoppingCartByProduct){
            each.customer?.name.let {
                c.add(customerRepository.findByName(each.customer?.name!!))
            }
        }
        return c
    }

    override fun getShoppingCartByNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findBySelectedProducts_Product_NameContainingIgnoreCase(name,PageRequest.of(page,pageSize))
    }

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getShoppingCart(): List<ShoppingCart> {
        return shoppingCartRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }

//    override fun getProductByName(name: String): Product {
//        return productRepository.findByName(name)
//
//    }

}
