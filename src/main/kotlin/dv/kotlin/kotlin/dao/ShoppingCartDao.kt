package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Customer
import dv.kotlin.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartDao{
    fun getShoppingCart():List<ShoppingCart>
    fun getShoppingCartByNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerByNameWithPage(name: String): List<Customer>
    fun findByProducrName(query: String): List<ShoppingCart>
}

