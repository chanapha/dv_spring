package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Manufacturer
import dv.kotlin.kotlin.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class ManufacturerDaolmpl:ManufacturerDao{

    override fun save(manufacturer: Manufacturer): Manufacturer {
        return manufacturerRepository.save(manufacturer)
    }

    override fun getManufacturers(): List<Manufacturer> {
        return mutableListOf(Manufacturer("Apple", "053123456"),
                Manufacturer("Samsung", "555666777888"),
                Manufacturer("CAMT", "0000000"))
    }

    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
}