package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Customer
import dv.kotlin.kotlin.entity.Product
import dv.kotlin.kotlin.entity.UserStatus
import dv.kotlin.kotlin.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBlmpl: CustomerDao{
    override fun findByStatus(status: String): List<Customer> {
        try {
            return customerRepository
                    .findByUserStatus(UserStatus.valueOf(status.toUpperCase()))
        }catch (e: IllegalArgumentException ){
            return emptyList()
        }
    }

    override fun getCustomerByAddress(address: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(address)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name,email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerRepository.findByNameEndingWithIgnoreCase(name)

    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomer(): List<Customer> {
        return customerRepository.findAll().filterIsInstance(Customer::class.java)
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findByName(name)
    }

}
