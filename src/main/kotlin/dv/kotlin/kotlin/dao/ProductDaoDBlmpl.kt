package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Product
import dv.kotlin.kotlin.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository


@Profile("db")
@Repository
class ProductDaoDBlmpl: ProductDao{
    override fun save(product: Product): Product {
        return productRepository.save(product)
    }

    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product> {
        return productRepository.findByNameContainingIgnoreCase(name,PageRequest.of(page, pageSize))
    }

    override fun getProductByManuName(name: String): List<Product> {
        return productRepository.findByManufacturer_NameContainingIgnoreCase(name)
    }

    override fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product> {
        return productRepository.findByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(name,desc)
    }

    override fun getProductByPartialName(name: String): List<Product> {
        return productRepository.findByNameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var productRepository: ProductRepository

    override fun getProduct(): List<Product> {
        return productRepository.findAll().filterIsInstance(Product::class.java)
    }

    override fun getProductByName(name: String): Product? {
        return productRepository.findByName(name)
    }

}
