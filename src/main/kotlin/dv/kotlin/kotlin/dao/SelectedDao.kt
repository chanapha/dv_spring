package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Product
import dv.kotlin.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedDao{
     fun getSelectedProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>

}