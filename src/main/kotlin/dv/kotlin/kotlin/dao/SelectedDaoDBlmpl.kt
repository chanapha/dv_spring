package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Product
import dv.kotlin.kotlin.entity.SelectedProduct
import dv.kotlin.kotlin.repository.SelectedProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SelectedDaoDBlmpl: SelectedDao{
    override fun getSelectedProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductRepository.findByProduct_NameContainingIgnoreCase(name, PageRequest.of(page, pageSize))

    }


    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository

}