package dv.kotlin.kotlin.dao

import dv.kotlin.kotlin.entity.Customer
import dv.kotlin.kotlin.entity.Product

interface CustomerDao{
    fun getCustomer():List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByAddress(address: String): List<Customer>
    fun findByStatus(status: String): List<Customer>

}
