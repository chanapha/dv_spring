package dv.kotlin.kotlin.controller

import dv.kotlin.kotlin.entity.dto.AddressDto
import dv.kotlin.kotlin.entity.dto.ManufacturerDto
import dv.kotlin.kotlin.util.MapperUtil
import dv.kotlin.kotlin.service.AddressService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class AddressController {

    @Autowired
    lateinit var addressService: AddressService


    @PostMapping("/address")
    fun addAddress(@RequestBody address: AddressDto)
            : ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapAddress(addressService.save(address))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()

//        return ResponseEntity.ok(
//                MapperUtil.INSTANCE.mapAddress(
//                        addressService.save(address)))
    }


    @PutMapping("/address/{manuId}")
    fun updateAddress(@PathVariable("addressId") id:Long?,
                           @RequestBody address: AddressDto)
            : ResponseEntity<Any>{

        address.id = id

        var output = MapperUtil.INSTANCE.mapAddress(addressService.save(address))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()

//        return ResponseEntity.ok(
//                MapperUtil.INSTANCE.mapAddress((
//                        addressService.save(address) ))
//        )

    }


}