package dv.kotlin.kotlin.controller

import dv.kotlin.kotlin.entity.dto.PageProductDto
import dv.kotlin.kotlin.entity.dto.PageSelectedProduct
import dv.kotlin.kotlin.service.SelectedService
import dv.kotlin.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController

class SelectedProductController{

    @Autowired
    lateinit var selectedService: SelectedService


    @GetMapping("/selectedProduct/name")
    fun getSelectedProductWithPage (@RequestParam("name") name: String,
                            @RequestParam("page") page: Int,
                            @RequestParam("pageSize")pageSize: Int): ResponseEntity<Any> {
        var output = selectedService.getSelectedProductWithPage(name,page,pageSize)
        return ResponseEntity.ok(PageSelectedProduct(totalPages = output.totalPages,
                totalElements = output.totalElements,
                selectedProducts = MapperUtil.INSTANCE.mapSelectedProductDto(output.content)))
    }

}