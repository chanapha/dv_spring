//package dv.kotlin.kotlin.controller
//
//import dv.kotlin.kotlin.entity.Person
//import dv.kotlin.kotlin.entity.Product
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//class SimpleController{
//
//
//    @GetMapping("/getAppName")
//    fun getHelloWorld():String{
//        return "assessment"
//    }
//
//    @GetMapping("/product")
//    fun getPerson(): ResponseEntity<Any>{
//        val product = Product("iPhone","A new telephone",28000,5)
//        return ResponseEntity.ok(product)
//    }
//
//    @GetMapping("/product/{name}")
//    fun getParam(@PathVariable("name") name:String ): ResponseEntity<Any>{
//        val product = Product(name,"A new telephone",28000,5)
//        if (name == "iPhone" ){
//            return ResponseEntity.ok(product)
//        }else{
//            return ResponseEntity.notFound().build()
//        }
//    }
//
//
//    @PostMapping("/setZeroQuantity")
//    fun setZeroQuantity(@RequestBody product: Product) : ResponseEntity<Any>{
//        product.quantity = 0
//        return ResponseEntity.ok(product)
//    }
//
//
//    @PostMapping("/totalPrice")
//    fun getTotalPrice(@RequestBody product: Array<Product>) : ResponseEntity<Any>{
//        var totalPrice: Int = 0
//        for (i: Product in product){
//            totalPrice = totalPrice + i.price
//        }
//            return ResponseEntity.ok(totalPrice)
//    }
//
//    @PostMapping("/avaliableProduct")
//    fun getAvaliableProduct(@RequestBody product: Array<Product>) : ResponseEntity<Any>{
//        var output = mutableListOf<Product>()
//        for (i: Product in product){
//            if(i.quantity != 0 ){
//                output.add(i)
//            }
//        }
//        return ResponseEntity.ok(output)
//    }
//
//
//
//
//
//
//
//
//
//
//
//
//
//    }