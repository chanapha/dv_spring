package dv.kotlin.kotlin.controller

import dv.kotlin.kotlin.entity.Person
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class HelloWorldController{
    @GetMapping("/helloWorld")
    fun getHelloWorld():String{
        return "HelloWorld"
    }

    @GetMapping("/person")
    fun getPerson(): ResponseEntity<Any>{
        val person = Person("somchai","somrak",15)
        return ResponseEntity.ok(person)
    }

    @GetMapping("/test")
    fun getTest():String{
        return "CAMT"
    }

    @GetMapping("/myPerson")
    fun getmyPerson(): ResponseEntity<Any>{
        val person = Person("Janey","Siri",100)
        return ResponseEntity.ok(person)
    }

    @GetMapping("/persons")
    fun getPersons(): ResponseEntity<Any>{
        val person01 = Person("somchai","somrak",15)
        val person02 = Person("Prayut","Chun",62)
        val person03 = Person("Lung","Pom",65)
        val persons = listOf<Person>(person01,person02,person03)
        return ResponseEntity.ok(persons)
    }

    @GetMapping("/params")
    fun getParams(@RequestParam("name") name: String, @RequestParam("surname" ) surname:String)
     :ResponseEntity<Any>{
        return ResponseEntity.ok("$name $surname")

    }

    @GetMapping("/param/{name}/{surname}/{age}")
    fun getPathParam(@PathVariable("name") name:String,
                     @PathVariable("surname") surname: String,
                     @PathVariable("age")age:Int): ResponseEntity<Any>{
        val person = Person(name,surname,age)
        return ResponseEntity.ok(person)
    }

    @PostMapping("/echo")
    fun echo(@RequestBody person: Person) : ResponseEntity<Any>{
        return ResponseEntity.ok(person)
    }




}


