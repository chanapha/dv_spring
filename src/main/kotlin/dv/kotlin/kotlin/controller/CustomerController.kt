package dv.kotlin.kotlin.controller

import dv.kotlin.kotlin.service.CustomerService
import dv.kotlin.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class CustomerController {

    @Autowired
    lateinit var customerService: CustomerService

    @GetMapping("/customer")
    fun getCustomer(): ResponseEntity<Any> {
        val customer = customerService.getCustomer()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customer))
    }

    @GetMapping("/customer/query")
    fun getCustomer(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/partialQuery")
    fun getCustomerPartial(@RequestParam("name") name: String,
                           @RequestParam("email", required = false) email: String?): ResponseEntity<Any> {

        val output:List<Any>

        if (email == null) {
            output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialName(name))
        }else{
            output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialNameAndEmail(name,email))
        }
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/address")
    fun getCustomerByAddress (@RequestParam("address") address: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByAddress(address))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/status")
    fun getCustomerFromStatus (@RequestParam(name = "status")status : String)
            : ResponseEntity<Any> {
        return ResponseEntity.ok(customerService.findByStatus(status))
    }

    @GetMapping("/customer/product")
    fun getCustomerFromBoughtProduct (@RequestParam(name = "name")name : String)
            : ResponseEntity<Any> {
        return ResponseEntity.ok(customerService.findByBoughtProduct(name))
    }



}
