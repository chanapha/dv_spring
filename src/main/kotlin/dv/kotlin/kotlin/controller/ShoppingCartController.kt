package dv.kotlin.kotlin.controller

import dv.kotlin.kotlin.entity.dto.PageProductDto
import dv.kotlin.kotlin.entity.dto.PageShoppingCartDto
import dv.kotlin.kotlin.service.ManufacturerService
import dv.kotlin.kotlin.service.ShoppingCartService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import dv.kotlin.kotlin.util.MapperUtil
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ShoppingCartController{

    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingcart")
    fun getShoppingCart(): ResponseEntity<Any> {
        val shoppingCart = shoppingCartService.getShoppingCart()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCart));
//        return ResponseEntity.ok(shoppingCart);
    }

    @GetMapping("/shoppingcart/name")
    fun getShoppingCartByNameWithPage (@RequestParam("name") name: String,
                            @RequestParam("page") page: Int,
                            @RequestParam("pageSize")pageSize: Int): ResponseEntity<Any> {
        var output = shoppingCartService.getShoppingCartByNameWithPage(name,page,pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingCart = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

//    @GetMapping("/customer/product")
//    fun getCustomerByNameWithPage(@RequestParam("name") name: String): ResponseEntity<Any> {
//        val output = shoppingCartService.getCustomerByNameWithPage(name)
//        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(output));
//    }







}