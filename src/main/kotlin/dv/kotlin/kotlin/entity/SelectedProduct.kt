package dv.kotlin.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class SelectedProduct(var quanlity: Int? = null){
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToOne
    var product: Product? = null
}