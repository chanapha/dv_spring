package dv.kotlin.kotlin.entity

enum class ShoppingCartStatus {
    WAIT,CONFIRM,PAID,SENT,RECEIVED
}