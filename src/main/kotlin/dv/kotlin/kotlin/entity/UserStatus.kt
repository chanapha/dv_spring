package dv.kotlin.kotlin.entity

enum class UserStatus {
    PENDING, ACTIVE, NOTACTIVE, DELETED
}
