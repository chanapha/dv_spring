package dv.kotlin.kotlin.entity.dto

import dv.kotlin.kotlin.entity.ShoppingCartStatus


data class ShoppingCartDto(var shoppingCartStatus: ShoppingCartStatus? = ShoppingCartStatus.WAIT,
                      var selectedProducts: List<SelectedProductDto>? = null,
                      var customer: CustomerDto?= null)