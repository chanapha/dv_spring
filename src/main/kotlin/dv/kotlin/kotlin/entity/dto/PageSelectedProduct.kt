package dv.kotlin.kotlin.entity.dto


data class PageSelectedProduct(
        var totalPages:Int? = null,
        var totalElements: Long? = null,
        var selectedProducts: List<SelectedProductDto> = mutableListOf()
)