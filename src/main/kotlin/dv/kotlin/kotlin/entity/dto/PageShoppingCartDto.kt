package dv.kotlin.kotlin.entity.dto

data class PageShoppingCartDto(
        var totalPages:Int? = null,
        var totalElements: Long? = null,
        var shoppingCart: List<ShoppingCartDto> = mutableListOf()
)