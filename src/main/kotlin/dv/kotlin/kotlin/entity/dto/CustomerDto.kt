package dv.kotlin.kotlin.entity.dto

import dv.kotlin.kotlin.entity.Address

data class CustomerDto(var name:String? = null,
                      var email: String? = null,
                      var UserStatus: String?= null,
                      var defaultAddress: Address? = null
)