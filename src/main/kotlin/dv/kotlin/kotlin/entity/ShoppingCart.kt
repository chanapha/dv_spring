package dv.kotlin.kotlin.entity

import javax.persistence.*

@Entity
data class ShoppingCart (var status: ShoppingCartStatus  = ShoppingCartStatus.WAIT){
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var selectedProducts = mutableListOf<SelectedProduct>()

    @ManyToOne
    lateinit var customer: Customer

    @ManyToOne
    lateinit var shipppingAddress: Address

}
