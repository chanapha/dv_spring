package dv.kotlin.kotlin.service

import dv.kotlin.kotlin.dao.ProductDao
import dv.kotlin.kotlin.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ProductServicelmpl: ProductService{
    override fun save(product: Product): Product {
        return productDao.save(product)
    }

    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product> {
        return productDao.getProductWithPage(name,page,pageSize)
    }

    override fun getProductByManuName(name: String): List<Product> {
        return productDao.getProductByManuName(name)
    }

    override fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product> {
        return productDao.getProductByPartialNameAndDesc(name,desc)
    }

    override fun getProductByPartialName(name: String): List<Product> {
        return productDao.getProductByPartialName(name)
    }


    @Autowired
    lateinit var productDao: ProductDao


    override fun getProductByName(name: String): Product?
        = productDao.getProductByName(name)


    override fun getProducts(): List<Product>{
        return productDao.getProduct()
    }


}