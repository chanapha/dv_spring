package dv.kotlin.kotlin.service

import dv.kotlin.kotlin.entity.Product
import dv.kotlin.kotlin.dao.SelectedDao
import dv.kotlin.kotlin.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service

class SelectedServicelmpl: SelectedService{
    override fun getSelectedProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedDao.getSelectedProductWithPage(name,page,pageSize)
    }

    @Autowired
    lateinit var selectedDao: SelectedDao

}