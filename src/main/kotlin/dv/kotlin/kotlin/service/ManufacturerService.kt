package dv.kotlin.kotlin.service

import dv.kotlin.kotlin.entity.Manufacturer
import dv.kotlin.kotlin.entity.dto.ManufacturerDto

interface ManufacturerService{
    fun getManufacturers(): List<Manufacturer>
    fun save(manu: ManufacturerDto): Manufacturer
}