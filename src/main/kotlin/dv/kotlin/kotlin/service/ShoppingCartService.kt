package dv.kotlin.kotlin.service

import dv.kotlin.kotlin.entity.Customer
import dv.kotlin.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartService{
    fun getShoppingCart(): List<ShoppingCart>
    fun getShoppingCartByNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerByNameWithPage(name: String): List<Customer>
}