package dv.kotlin.kotlin.service

import dv.kotlin.kotlin.dao.CustomerDao
import dv.kotlin.kotlin.dao.ShoppingCartDao
import dv.kotlin.kotlin.entity.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CustomerServicelmpl: CustomerService{
    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun findByBoughtProduct(name: String): List<Customer> {
        return shoppingCartDao.findByProducrName(name)
                .map { shoppingCart -> shoppingCart.customer }
                .toSet().toList()
    }

    override fun findByStatus(status: String): List<Customer> {
        return customerDao.findByStatus(status)
    }

    override fun getCustomerByAddress(address: String): List<Customer> {
        return customerDao.getCustomerByAddress(address)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByPartialNameAndEmail(name,email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }

    @Autowired
    lateinit var customerDao: CustomerDao


    override fun getCustomerByName(name: String): Customer?
            = customerDao.getCustomerByName(name)

    override fun getCustomer(): List<Customer>{
        return customerDao.getCustomer()
    }

}