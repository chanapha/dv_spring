package dv.kotlin.kotlin.service

import dv.kotlin.kotlin.dao.AddressDao
import dv.kotlin.kotlin.entity.Address
import dv.kotlin.kotlin.entity.dto.AddressDto
import dv.kotlin.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressServicelmpl: AddressService{
    override fun save(address: AddressDto): Address {
        val address = MapperUtil.INSTANCE.mapAddress(address)
        return addressDao.save(address)

    }

    @Autowired
    lateinit var addressDao: AddressDao


}