package dv.kotlin.kotlin.service

import dv.kotlin.kotlin.dao.ShoppingCartDao
import dv.kotlin.kotlin.entity.Customer
import dv.kotlin.kotlin.entity.ShoppingCart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ShoppingCartServicelmpl: ShoppingCartService{
    override fun getCustomerByNameWithPage(name: String): List<Customer> {
        return shoppingCartDao.getCustomerByNameWithPage(name)
    }

    override fun getShoppingCartByNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartByNameWithPage(name, page, pageSize)
    }

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao
    override fun getShoppingCart(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCart()   }
}

