package dv.kotlin.kotlin.service

import dv.kotlin.kotlin.dao.ManufacturerDao
import dv.kotlin.kotlin.entity.Manufacturer
import dv.kotlin.kotlin.entity.dto.ManufacturerDto
import dv.kotlin.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManufacturerServicelmpl: ManufacturerService{

    override fun save(manu: ManufacturerDto): Manufacturer {
        val manufacturer = MapperUtil.INSTANCE.mapManufacturer(manu)
        return manufacturerDao.save(manufacturer)
    }


    @Autowired
    lateinit var manufacturerDao: ManufacturerDao
    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getManufacturers()   }
}