package dv.kotlin.kotlin.service
import dv.kotlin.kotlin.entity.Address
import dv.kotlin.kotlin.entity.dto.AddressDto


interface AddressService{
     fun save(address: AddressDto): Address

}