package dv.kotlin.kotlin.service

import dv.kotlin.kotlin.entity.Customer

interface CustomerService{
    fun getCustomer(): List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByAddress(address: String): List<Customer>
    fun findByStatus(status: String): List<Customer>
    fun findByBoughtProduct(name: String): List<Customer>

}