package dv.kotlin.kotlin.service

import dv.kotlin.kotlin.entity.Product
import dv.kotlin.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedService{
     fun getSelectedProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>

}